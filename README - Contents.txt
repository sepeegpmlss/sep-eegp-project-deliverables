This folder contains the following documents:

EEGP
  EEGP Design Document
    - Provides for each feature:
       - Actor Diagrams
       - Use Case Narratives
       - Activity Diagram
       - Sequence Diagram
       - Wireframe Diagram
  EEGP User Manual
  EEGP Software Requirements Specification
  EEGP Installation and Deployment Guide
  EEGP User Acceptance Test Plan
  EEGP User Acceptance Test Summary Report
  EEGP Training Plan
  EEGP Database Documentation
  EEGP Code Documentation
  EEGP Code
  EEGP App - 1.6.4

SEP
   SEP Design Document
    - Provides for each feature:
       - Actor Diagrams
       - Use Case Narratives
       - Activity Diagram
       - Sequence Diagram
       - Wireframe Diagram
   SEP User Manual
   SEP Software Requirements Specification
   SEP Installation and Deployment Guide
   SEP User Acceptance Test Plan
   SEP User Acceptance Test Summary Report
   SEP Training Plan
   SEP Database Documentation
   SEP Code Documentation
   SEP Code
   SEP App - 1.6.4

Technical Training Resources
  Lecture Slides used in Technical Training